'use strict';

angular.module('awesome-app.common.components', [
    'awesome-app.common.components.tabs',
    'awesome-app.common.components.teamList',
    'awesome-app.common.components.typeahead',
    'awesome-app.common.components.table'
]);
