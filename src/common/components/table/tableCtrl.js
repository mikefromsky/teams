'use strict';

angular.module('awesome-app.common.components.table').

controller('TableCtrl', 
    ['$scope', 'ngTableParams', 'Employees', '$filter', 'TeamService', 
    function($scope, ngTableParams, Employees, $filter, TeamService) {
    
    $scope.teamService = TeamService;
    $scope.teamSelected = {};

    $scope.$watch('teamService.getSelectedTeam()', function(newVal) {
        $scope.teamSelected = newVal;
    });
    
    Employees.getEmployees().then(function(data) {
        $scope.$data = data;
        $scope.tableParams = new ngTableParams(
            {
                page: 1,
                count: 10        
            }, 
            {
                total: $scope.$data.length,
                getData: function($defer, params) {
                    var orderedData = params.filter() ?
                        $filter('filter')(data, params.filter()) :
                        data;
                    $scope.data = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    params.total(orderedData.length);
                    $defer.resolve($scope.data);
                }
            }
        );
    })

    $scope.addMember = function(member, $event) {
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
        TeamService.addEmployeesToSelectedTeam(member);
    }

}]);
