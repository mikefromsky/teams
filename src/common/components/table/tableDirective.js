'use strict';

angular.module('awesome-app.common.components.table')

.directive('awesomeTable', ['Employees',
    function(Employees) {

        return {
            restrict: 'E',
            scope: {},
            controller: 'TableCtrl',
            templateUrl: '../common/components/table/table.tpl.html',
            link: function($scope) {

            }
        }

    }
]);
