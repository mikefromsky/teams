'use strict';

angular.module('awesome-app.common.components.typeahead').

controller('TypeaheadCtrl', ['$scope', '$rootScope', 'TeamService',
    function($scope, $rootScope, TeamService) {

        $scope.selectedEmployees = [];
        $scope.teamService = TeamService;
        $scope.selectedTeam = {};

        function refreshInput() {
            if ($scope.selectedTeam) {
                $scope.selectedEmployees = [];
                for (var i = 0; i < $scope.selectedTeam.employees.length; i++) {
                    $scope.selectedEmployees.push($scope.selectedTeam.employees[i].name);
                }
            }
        }

        $scope.$watch('teamService.getSelectedTeam()', function(team) {
            $scope.selectedTeam = team;
            refreshInput()
        });

        $rootScope.$on('teamChanged', function() {
            refreshInput();
        });

        $scope.refresh = function() {
            TeamService.refreshTeam($scope.selectedEmployees);
        };

    }
]);
