'use strict';

angular.module('awesome-app.common.components.typeahead')

.directive('awesomeTypeahead', ['Employees',
    function(Employees) {

        return {
            restrict: 'E',
            transclude: true,
            scope: {},
            controller: 'TypeaheadCtrl',
            templateUrl: '../common/components/typeahead/typeahead.tpl.html',
            link: function($scope) {
                Employees.getEmployees().then(function(data) {
                    $scope.employees = data;
                })
            }
        }

    }
]);
