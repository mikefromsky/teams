'use strict';

angular.module('awesome-app.common.components.table').

controller('TeamListCtrl', ['$scope', '$rootScope', 'TeamModel', 'TeamCollection', 'Employees', 'TeamService', 'CollectionName',
    function($scope, $rootScope, TeamModel, TeamCollection, Employees, TeamService, CollectionName) {

        var teamCollection = new TeamCollection(CollectionName);
        $scope.teamCollection = teamCollection.models;

        $scope.addTeam = function(name) {
            var team = new TeamModel(name);
            teamCollection.addTeam(team);
        };

        $scope.selectedTeam = [];

        $scope.selectTeam = function() {
            TeamService.setSelectedTeam($scope.selectedTeam[0]);
        };

        $scope.removeMember = function(id) {
            TeamService.removeEmployeeFromSelectedTeam(id);
            $rootScope.$broadcast("teamChanged")
        }

    }
]);
