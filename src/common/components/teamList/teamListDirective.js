'use strict';

angular.module('awesome-app.common.components.teamList')

.directive('awesomeTeamList', [
    function() {

        return {
            restrict: 'E',
            scope: {},
            templateUrl: '../common/components/teamList/teamList.tpl.html',
            controller: 'TeamListCtrl'
        }

    }
]);
