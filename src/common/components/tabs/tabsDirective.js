'use strict';

angular.module('awesome-app.common.components.tabs')

.directive('awesomeTabs', ["$state",
    function($state) {

        return {
            restrict: 'E',
            transclude: true,
            scope: {},
            templateUrl: '../common/components/tabs/tabs.tpl.html',
            link: function linkFn($scope, element, attrs) {
                $scope.tabs = [{
                    heading: "Add member",
                    route: "find",
                    active: true
                }, {
                    heading: "All employees",
                    route: "choose",
                    active: false
                }]
                $scope.go = function(route) {
                    $state.go(route);
                };
                $scope.active = function(route) {
                    return $state.is(route);
                };
                $scope.$on("$stateChangeSuccess", function() {
                    $scope.tabs.forEach(function(tab) {
                        tab.active = $scope.active(tab.route);
                    });
                });
            }
        };

    }
]);
