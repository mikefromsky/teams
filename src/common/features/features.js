'use strict';

angular.module('awesome-app.common.features', [
    'awesome-app.common.features.abstract-entity',
    'awesome-app.common.features.employees',
    'awesome-app.common.features.team'
]);
