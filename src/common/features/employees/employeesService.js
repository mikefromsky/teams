'use strict';

angular.module('awesome-app.common.features.employees').

factory('Employees', ['$q', '$http', function($q, $http) {

    return {
    
        employees: [],
    
        getEmployees: function() {
            var self = this;
            var deferred = $q.defer();
            $http.get('assets/staff.json').then(function(response) {
                self.employees = response.data;
                deferred.resolve(response.data);
            }, function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        },
    
        findInGeneral: function(name) {
            for (var i = 0; i < this.employees.length; i++) {
                if (this.employees[i].name == name) {
                    return this.employees[i];
                }
            }
            return false;
        }
    
    };

}])
