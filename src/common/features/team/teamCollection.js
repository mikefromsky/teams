'use strict';

angular.module('awesome-app.common.features.team').

factory('TeamCollection', ['AbstractEntity', 'TeamModel',
    function(AbstractEntity, TeamModel) {

        var TeamCollection = AbstractEntity.extend({

            initialize: function(collectionName) {
                this.collectionName = collectionName;
                this.models = [];
            },

            addTeam: function(team) {
                var self = this;
                if (!team instanceof TeamModel) {
                    return;
                }
                var repeated = false;
                self.models.forEach(function(obj, i) {
                    if (obj.name === team.name) {
                        repeated = true;
                    }
                });
                if (!repeated) {
                    this.models.push(team);
                }
                return this;
            }

        });

        return TeamCollection;
    }
]);
