'use strict';

angular.module('awesome-app.common.features.team').

factory('TeamModel', ['AbstractEntity', 'Employees',
    function(AbstractEntity, Employees) {

        var TeamModel = AbstractEntity.extend({

            initialize: function(name) {
                this.name = name;
                this.employees = [];
            },

            addEmployees: function(employees) {
                var self = this;
                if (Object.prototype.toString.call(employees) !== '[object Array]') {
                    var repeated = false;
                    self.employees.forEach(function(obj) {
                        if (obj.name === employees.name) {
                            repeated = true;
                        }
                    });
                    if (repeated) {
                        return;
                    }
                    self.employees.push(employees);
                    return;
                }
                employees.forEach(function(name) {
                    var repeated = false;
                    self.employees.forEach(function(obj) {
                        if (obj.name === name) {
                            repeated = true;
                        }
                    })
                    if (repeated) {
                        return;
                    }
                    var found = Employees.findInGeneral(name);
                    if (found) {
                        self.employees.push(found);
                    }
                })
            },

            removeEmployee: function(employeeId) {
                var employeesLength = this.employees.length;
                for (var i = 0; i < employeesLength; i++) {
                    if (this.employees[i].id === employeeId) {
                        this.employees.splice(i, 1);
                        return true;
                    }
                }
                return false;
            },

            refresh: function(employeeNames) {
                var self = this;
                this.employees = [];
                employeeNames.forEach(function(obj) {
                    var found = Employees.findInGeneral(obj);
                    if (found) {
                        self.employees.push(found);
                    }
                })
            }

        });

        return TeamModel;

    }
]);
