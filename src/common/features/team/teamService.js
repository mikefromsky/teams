'use strict';

angular.module('awesome-app.common.features.team').

service('TeamService', ['TeamModel', 'TeamCollection', 'Employees',
    function(TeamModel, TeamCollection, Employees) {

        var selectedTeam = undefined;

        return {

            setSelectedTeam: function(team) {
                selectedTeam = team;
            },

            getSelectedTeam: function() {
                return selectedTeam;
            },

            addEmployeesToSelectedTeam: function(employees) {
                if (!selectedTeam) {
                    return;
                }
                selectedTeam.addEmployees(employees);
            },

            removeEmployeeFromSelectedTeam: function(employeeId) {
                selectedTeam.removeEmployee(employeeId)
            },

            refreshTeam: function(employees) {
                selectedTeam.refresh(employees);
            }

        }
    }
]);
