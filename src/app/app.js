'use strict';

angular.module('awesome-app', [
    'ui.router',
    'ui.bootstrap',
    'ngSanitize',
    'templates-app',
    'awesome-app.common',
    'angular_taglist_directive',
    'ngTable',
    'selectionModel'
]).

value("CollectionName", "Default").

config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {

    $stateProvider
        .state('find', {
            url: '/find',
            templateUrl: 'find/find.tpl.html'
        })
        .state('choose', {
            url: '/choose',
            templateUrl: 'choose/choose.tpl.html'
        });

    $urlRouterProvider.otherwise('/find');

}]);
